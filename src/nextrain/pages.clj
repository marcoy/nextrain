(ns nextrain.pages
  (:use [hiccup.page :only (html5 include-css include-js)]
        [hiccup.element :only (javascript-tag)]
        hiccup.core))

(defn layout [& {:keys [body] :as params}]
  (html5
    [:head
      [:title "NexTrain"]
      [:meta {:name "viewport" :content "width=device-width, initial-scale=1"}]
      [:meta {:name "apple-mobile-web-app-capable" :content "yes"}]
      (include-css "http://code.jquery.com/mobile/1.1.0/jquery.mobile-1.1.0.min.css")
      (include-js ;"https://www.google.com/jsapi"
                  "/js/jquery-1.7.2.min.js"
                  "/js/jquery.mobile-1.1.0.min.js"
                  "/js/mustache-0.5.1-dev.js"
                  "/js/underscore-1.3.3.min.js"
                  "/js/nextrain.js")
      (when-let [scripts (:scripts params)]
        (for [script scripts]
          (include-js script)))]
    [:body body]))

