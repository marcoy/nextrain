(ns nextrain.snippets
  (:use hiccup.core
        [clojure.pprint :only [pprint]]))

(defn mobile-page
  [& {:as params}]
  (html [:div {:data-role "page" :id (:id params)}
         [:div {:data-role "header"}
           (:header params)]
         [:div {:data-role "content"}
           (:content params)]]))

(defn default-lat-lon []
  (html [:div {:data-role "page"}
         [:div {:data-role "header"}
           [:h1 "Your Location"]]
         [:div {:data-role "content"}
           [:div#lat "latitude"]
           [:div#lon "longitude"]]]))

(defn index-mobile-page []
  (mobile-page :id "indexpage"
               :header (html [:h1 "Nearest Stops"]
                             [:a {:href "/allstops"} "Stops"])
               :content (html [:div#stops-list-template
                                [:ul#stops-list {:data-role "listview"}]])))

(defn stop-mobile-page [stop]
  (println stop)
  (mobile-page :id "stoppage"
               :header  (html [:h1 (:stop_name stop)])
               :content (html [:div#times-list-template
                               [:ul#times-list {:data-role "listview"}]])))

(defn all-stops-mobile-page [stops]
  (mobile-page :id "allstopspage"
               :header (html [:h1 "Stops"])
               :content (html [:div
                               [:ul#all-stops-list {:data-role "listview"
                                                    :data-filter "true"
                                                    :data-filter-placeholder "Search Stop ..."}
                                (for [stop stops]
                                  [:li
                                   [:a {:href (str "/stop/" (:stop_id stop))}
                                    (:stop_name stop)]])]])))
