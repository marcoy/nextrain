(ns nextrain.core
  (:use compojure.core
        nextrain.controller
        ring.adapter.jetty)
  (:require [compojure.route :as route]
            [compojure.handler :as handler]))

(defn testsession [params session]
  (println params)
  (println session)
  (if-let [n (:n session)]
    {:body (str session)
     :session (assoc session :n (inc n))}
    {:body (str session)
     :session (assoc session :n 1)}))

(defroutes main-routes
  (GET "/" [] (index))
  (GET "/stop/:id" [id] (stop id))
  (GET "/allstops" [] (all-stops))
  (POST "/findstops" {params :params} (find-stops params))
  (POST "/traintimes" {params :params} (next-train-times params))
  (GET "/ses" {params :params
               session :session} (testsession params session))
  (route/resources "/")
  (route/not-found "Page not found"))

;(def app
;  (-> #'main-routes handler/site))
(def app (handler/site main-routes))

(defn -main [& args]
  (run-jetty #'app {:port 3000 :join? true}))

