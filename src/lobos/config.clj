(ns lobos.config
  (:use lobos.connectivity
        [korma.db :only [sqlite3 mysql]]))

(def gtfsdb (sqlite3 {:db "gtfs.db"}))
(open-global gtfsdb)

