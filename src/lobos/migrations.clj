(ns lobos.migrations
  (:refer-clojure :exclude [alter drop
                            bigint boolean char double float time])
  (:use (lobos connectivity core schema config
               [migration :only [defmigration]])))

(defmigration add-stops-table
  (up [] (create
          (table :stops
                 (integer :stop_id)
                 (varchar :stop_name 192))))
  (down [] (drop (table :stops))))
