(defproject nextrain "0.0.1"
  :description "When is the next train!?"
  :url "http://marcoyuen.com"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :plugins      [[lein-ring  "0.7.1"]
                 [lein-swank "1.4.4"]
                 [lein-midje "1.0.10"]]
  :dependencies [[org.clojure/clojure        "1.3.0"]
                 [org.clojure/tools.trace    "0.7.3"]
                 [org.clojure/data.json      "0.1.3"]
                 [org.xerial/sqlite-jdbc     "3.7.2"]
                 [mysql/mysql-connector-java "5.1.20"]
                 [compojure                  "1.1.0"]
                 [hiccup                     "1.0.0"]
                 [ring-json-response         "0.2.0"]
                 [useful                     "0.8.3-alpha2"]
                 [korma                      "0.3.0-beta11"]
                 [lobos                      "1.0.0-SNAPSHOT"]]
  :dev-dependencies [[clojure-source "1.3.0"]
                     [midje          "1.4.0"]
                     [ring-serve     "0.1.2"]
                     [ring-mock      "0.1.2"]]
  :resources-path "resources"
  :main nextrain.core
  :ring {:handler nextrain.core/app})
