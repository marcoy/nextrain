# $(document).bind("pagebeforeload pageload pageloadfailed pagebeforechange pagechange pagechangefailed pagebeforeshow pagebeforehide pageshow pagehide pagebeforecreate pagecreate pageinit pageremove updatelayout", (e) -> console.log e.type)

nextrainapp = {}
((app) ->
    app.templates = {
      'stopslistTemplate':
        "{{#stop}}<li><a href='{{stop_url}}'>{{stop_name}}</a></li>{{/stop}}"
      'timeslistTemplate':
        "<li>{{trip_headsign}}<span class='ui-li-aside'>{{departure_time}}</span></li>"
    }

    app.locSuccess = (position)->
        console.log position
        $.post('/findstops',
               {lat: position.coords.latitude, lon: position.coords.longitude},
               (data, status, jqxhr) ->
                   $stopsList = $('#stops-list')
                   content = Mustache.render(app.templates['stopslistTemplate'], data)
                   $stopsList.append(content)
                   $stopsList.listview('refresh')
                   console.log(data))

    app.locError = (error) ->
        console.log("Cannot acquire location.")
        switch error.code
            when error.PERMISSION_DENIED then alert "User did not share location data."
            when error.POSITION_UNAVAILABLE then alert "Could not detect current position"
            when error.TIMEOUT then alert "Retrieving position timed out"
            else alert "Unknown error"

    app.bindings = ->
        $('#indexpage').live('pagebeforecreate',
            (event) ->
                if navigator.geolocation
                    navigator.geolocation.getCurrentPosition(app.locSuccess, app.locError,
                                                             {enableHighAccuracy: true,
                                                             maximumAge: 6000,
                                                             timeout: 30000})
        )
        $('#stoppage').live('pagebeforecreate',
            (event) ->
                stop_id   = _.last(event.target.baseURI.split('/'))
                timestamp = new Date().getTime()
                $.post('/traintimes',
                      {timestamp: timestamp, stop_id: stop_id},
                      (data, status, jqxhr) ->
                        $timesList = $('#times-list')
                        for time in data.times
                          do (time) ->
                            content = Mustache.render(app.templates['timeslistTemplate'], time)
                            $timesList.append(content)
                        $timesList.listview('refresh')
                      )
        )
    app.init = -> app.bindings()
    app.init()) nextrainapp

